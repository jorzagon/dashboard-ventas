fetch('http://127.0.0.1:3000/dato-unificado')
    .then(respuesta => respuesta.json())
    .then((data) => {
        let seriesData = []

        // Recorrido del objeto y de sus listas
        for (var key in data.cantidad) {
            if (data.cantidad.hasOwnProperty(key)) {
                // Obtener el valor en la posición 11 de cada lista
                let valor = data.cantidad[key][11];
                // Agregar el valor a la serie de datos
                seriesData.push(valor);
            }
        }
        
        var options = {
            chart: {
                type: 'line',
                zoom: {
                    enabled: false
                },
                toolbar: {
                    show: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth',
                width: 2
            },
            markers: {
                size: 5
            },
            series: [{
                name: 'Operaciones Total',
                data: seriesData
            }],
            xaxis: {
                categories: data.mesesUnicos,
                labels: {
                    style: {
                        colors: '#fff'
                    }
                }
            },
            yaxis: {
                labels: {
                    show: false
                }
            }
        }

        var chart = new ApexCharts(document.querySelector("#total-operacion"), options);
        chart.render();
    })