fetch('http://localhost:3000/dato-unificado')
    .then(respuesta => respuesta.json())
    .then((data) => {

        const mesesUnicos = data.mesesUnicos;
        const meses = Object.keys(data.cantidad);
        const operaciones = data.operaciones;
        const series = operaciones.map(operacion => {
            const valoresOperacion = meses.map(mes => data.cantidad[mes][operaciones.indexOf(operacion)]);
            //return [operacion, valoresOperacion];
            return valoresOperacion
    });

        console.log(meses)

        var options = {
            series: [{
                name: operaciones[1],
                data: series[1]
            }, {
                name: operaciones[2],
                data: series[2]
            }, {
                name: operaciones[4],
                data: series[4]
            }],
            chart: {
                type: 'bar',
                width: 450,
                height: 210
            },
            legend: {
                labels: {
                    colors: '#c4c7df'
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: mesesUnicos,
                labels: {
                    style: {
                        colors: '#fff'
                    }
                }
            },
            yaxis: {
                title: {
                    text: '$ (thousands)',
                    style: {
                        color: '#fff'
                    }
                },
                labels: {
                    style: {
                        colors: '#fff'
                    }
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return "$ " + val + " thousands"
                    }
                }
            }
        };

        var chart = new ApexCharts(document.querySelector("#migraciones"), options);
        chart.render();
    })

