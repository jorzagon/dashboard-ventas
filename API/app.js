import express from "express";
import XLSX from "xlsx";
const app = express();

// Middleware para configurar las cabeceras CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.get('/dato', (pedido, respuesta) => {
    const workbook = XLSX.readFile('../BD/salida.xlsx');
    const sheetName = workbook.SheetNames[0];
    const sheet = workbook.Sheets[sheetName];
    const jsonData = XLSX.utils.sheet_to_json(sheet, { header: 1 });
    respuesta.json(jsonData);
});

app.get('/dato-unificado', (pedido, respuesta) => {
    const workbook = XLSX.readFile('../BD/BD_ventas.xlsx');
    const nombreHoja = workbook.SheetNames[2];
    const hoja = workbook.Sheets[nombreHoja];
    const jsonDatos = XLSX.utils.sheet_to_json(hoja, { header: 1 });

    const datos = {
        cantidad: { enero: [], febrero: [] },
        operaciones: [],
        meses: [],
        mesesUnicos: []
    };

    jsonDatos.forEach((fecha) => {
        switch (fecha[0]) {
            case 45292:
                fecha[0] = '2024/01';
                datos.meses.push(fecha[0]);
                datos.operaciones.push(fecha[1]);
                datos.cantidad.enero.push(fecha[2]);
                break;
            case 45323:
                fecha[0] = '2024/02';
                datos.meses.push(fecha[0]);
                datos.cantidad.febrero.push(fecha[2])
                break;
            default:
                break;
        }
    })

    // Busca si el valor esta en la lista 'mesesUnicos' y si no esta lo ingresa
    // indexOf devuelve el índice del valor que se encuentra en el array 'mesesUnicos'
    // si el valor no esta devuelve -1
    for (let i = 0; i < datos.meses.length; i++) {
        if (datos.mesesUnicos.indexOf(datos.meses[i]) === -1) {
            datos.mesesUnicos.push(datos.meses[i])
        }
    }

    //datos.meses = []
    respuesta.json(datos);
})

app.get('/dato-operaciones', (pedido, respuesta) => {
    const workbook = XLSX.readFile('../BD/BD_ventas.xlsx');
    // const nombreHoja = workbook.SheetNames[2];
    const hoja = workbook.Sheets['Operaciones'];
    const jsonDatos = XLSX.utils.sheet_to_json(hoja, { header: 1 });

    const datos = [[], [], [],
    ['enero-2024', 'febrero-2024']
    ];


    respuesta.json(datos);
})

app.listen(3000, () => {
    console.log('Servidor iniciado en http://localhost:3000');
});