async function tabla() {
    try {
        const respuesta = await fetch('http://localhost:3000/dato', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if(!respuesta.ok) {
            throw new Error($`Error en la solicitud ${respuesta.status}`);
        }
        const data = await respuesta.json();
        const contenedorExcel = document.getElementById('excelData');
        const tabla = document.createElement('table');

        data.forEach(fila => {
            const tr = document.createElement('tr');
            fila.forEach(datoCelda => {
                const td = document.createElement('td');
                td.textContent = datoCelda;
                tr.appendChild(td);
            });
            tabla.appendChild(tr);
        });

        contenedorExcel.appendChild(tabla);

    } catch (error) {
        console.log('Error al obtener los datos del servidor: ', error);
        throw error;
    }
}