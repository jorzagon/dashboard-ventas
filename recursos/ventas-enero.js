async function ventasEnero() {
    try {
        const respuesta = await fetch('http://localhost:3000/dato')
        const datos = await respuesta.json();

        if(!respuesta.ok) {
            throw new Error($`Erro al conectar con la base de datos: ${respuesta.status}`)
        };

        return datos;
    } catch (error) {
        console.log($`Error con el servidor ${error}`);
    }
}

export default ventasEnero;