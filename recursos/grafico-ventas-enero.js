import ventasEnero from "./ventas-enero.js";

document.addEventListener('DOMContentLoaded', async function () {
    const datos = await ventasEnero();
    let linea = {
        chart: {
            type: 'line',
            zoom: {
                enable: false
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth',
            width: 2
        },
        markers: {
            size: 5
        },
        series: [{
            name: 'sales',
            data: datos[1]
        }],
        xaxis: {
            categories: datos[0],
            labels: {
                style: {
                    colors: '#f67'
                }
            }
        }
    }
    var chart = new ApexCharts(document.querySelector("#ventas"), linea).render();

    console.log(datos)
})