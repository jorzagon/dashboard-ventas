/*
let seriesData = []

// Recorrido del objeto y de sus listas
for (var key in data.operaciones) {
    if (data.operaciones.hasOwnProperty(key)) {
        // Obtener el valor en la posición 11 de cada lista
    let op = data.operaciones[key];
    // Agregar el valor a la serie de datos
    seriesData.push(op);
    }
}

--------------------------------------------------------------------------------------

*** Por operacion muestra los valores de todas las operaciones y todos los meses ***

const meses = Object.keys(data.cantidad);
const operaciones = data.operaciones;
const series = operaciones.map(operacion => ({
    name: operacion,
    data: meses.map(mes => [mes, data.cantidad[mes][operaciones.indexOf(operacion)]])
}));

console.log(series);

--------------------------------------------------------------------------------------

*** Por cada operación muestra los valores correspondientes de cada mes ***

const meses = Object.keys(data.cantidad);
const operaciones = data.operaciones;
const series = operaciones.map(operacion => ({
    name: operacion,
    data: meses.map(mes => data.cantidad[mes][operaciones.indexOf(operacion)])
}));

console.log(series);
*/