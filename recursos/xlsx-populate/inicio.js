import XlsxPopulate from 'xlsx-populate';

/*
***** fromBlankAsync() *****
- Escribir una celda:
workbook.sheet(0).cell('A1').value('Hola mundo');

- Escribir un rango de celdas:
workbook.sheet(0).cell('A1').value([
    ['Nombre', 'Apellido', 'DNI'],
    ['Jorge', 'González', 31601405]
]);

***** fromFileAsync("./salida.xlsx") *****
- Leer por celda:
const valor = workbook.sheet('Sheet1').cell('A1').value();

- Leer todo, devuelve arreglos por filas:
const valor = workbook.sheet('Sheet1').usedRange().value();

- Leer por rango:
const valor = workbook.sheet('Sheet1').range('A1:B2').value();

- Crear una nueva hoja en el archivo existente:
workbook.addSheet("Hoja 2");
workbook.toFileAsync("./salida.xlsx");
*/


// Escritura de archivo
async function principal() {
    // El método 'fromBlankAsync()' crea una archivo nuevo
    const workbook = await XlsxPopulate.fromBlankAsync();
    workbook.sheet(0).cell('A1').value('Hola mundo');
    workbook.sheet(0).cell('A2').value('Desde XLSX Populate');
    workbook.sheet(0).cell('B1').value('Nombre');
    workbook.toFileAsync("./salida.xlsx");
};

// Lectura de archivo
async function lectura() {
    // El método 'fromFileAsync()' lee el archivo
    const workbook = await XlsxPopulate.fromFileAsync("./salida.xlsx");
    const valor = workbook.sheet(0).cell('B2').value();
    console.log(valor)
}

// Lectura de archivo online
async function lecturaOnline() {
    try {
        const respuesta = await fetch('https://telefonicacorp-my.sharepoint.com/:x:/r/personal/jorgeluis_gonzalez2_telefonica_com/Documents/CalendarioVacaciones.xlsx?d=w64d329291efa411dae921ac512bd8f02&csf=1&web=1&e=qNEdPK');

        // Verificar si la solicitud fue exitosa
        if (!respuesta.ok) {
            throw new Error(`Error al cargar el archivo (${respuesta.status} ${respuesta.statusText})`);
        }

        // Convierte el contenido de la respuesta en un array buffer
        const buffer = await respuesta.arrayBuffer();

        // Lee el archivo Excel desde el buffer
        const workbook = await XlsxPopulate.fromDataAsync(buffer);

        const valor = workbook.sheet(0).cell('B3').value();
        console.log(valor);
    } catch (error) {
        console.error('Error al leer el archivo Excel en línea:', error);
    }
};

// principal();
// lectura();
// lecturaOnline();