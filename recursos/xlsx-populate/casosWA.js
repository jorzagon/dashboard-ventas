import datos from "../API/index.js";
import XlsxPopulate from "xlsx-populate";

async function mostrarDatos() {
    try {
        // Importa los datos
        /* 
        const datos = await import('../API/index.js');
        const valores = await datos.default();
        */

        const workbook = await XlsxPopulate.fromFileAsync('../BD/salida.xlsx');
        const valores = workbook.sheet('Sheet1').usedRange().value();

        // Agrega los datos a la página HTML
        // NO SE MUESTRA EN EL HTML. Error de consola:
        // Uncaught TypeError: Failed to resolve module specifier "xlsx-populate". Relative references must start with either "/", "./", or "../".
        const cuerpo = document.getElementById('datos-container');
        valores.forEach(valor => {
            const parrafo = document.createElement('p');
            parrafo.textContent = valor;
            cuerpo.appendChild(parrafo);
        });
    } catch (error) {
        console.error('Error al obtener los datos:', error);
    }
}

// Llama a la función cuando se cargue la página
window.addEventListener('DOMContentLoaded', mostrarDatos);